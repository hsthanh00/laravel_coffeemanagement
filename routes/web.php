<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('signin','AccountController@signin')->name('signin');
Route::post('login','AccountController@login')->name('login');

Route::get('signup','AccountController@signup')->name('signup');
Route::post('join','AccountController@join')->name('join');

Route::get('forgot','AccountController@forgot')->name('forgot');
Route::post('sentpassword','AccountController@sentpassword')->name('sentpassword');


Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function()
{
    Route::prefix('table')->name('table.')->group(function()
    {
        Route::get('index','TableController@index')->name('index');
        Route::post('index/{id}','TableController@AddFood')->name('AddFood');

        Route::get('create','TableController@create')->name('create');
        Route::post('store','TableController@store')->name('store');

        Route::get('edit/{id}','TableController@edit')->name('edit');
        Route::post('update/{id}','TableController@update')->name('update');

        Route::get('destroy/{id}','TableController@destroy')->name('destroy');

        Route::get('pay/{id}','TableController@pay')->name('pay');

        Route::get('cancle/{id}','TableController@cancle')->name('cancle');

    });
});

Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function()
{
    Route::prefix('category')->name('category.')->group(function()
    {
        Route::get('index/{table_id}','CategoryController@index')->name('index');
        
        Route::get('create','CategoryController@create')->name('create');
        Route::post('store','CategoryController@store')->name('store');

        Route::get('edit/{id}','CategoryController@edit')->name('edit');
        Route::post('update/{id}','CategoryController@update')->name('update');

        Route::get('destroy/{id}','CategoryController@destroy')->name('destroy');
    });
});



Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function()
{
    Route::prefix('product')->name('product.')->group(function()
    {
        Route::get('index/{category_Id}','ProductController@index')->name('index');
        
        Route::get('create/{category_Id}','ProductController@create')->name('create');
        Route::post('store','ProductController@store')->name('store');

        Route::get('edit/{id}','ProductController@edit')->name('edit');
        Route::post('update/{id}','ProductController@update')->name('update');

        Route::get('destroy/{id}','ProductController@destroy')->name('destroy');

        Route::get('addfood/{table_id}','ProductController@addfood')->name('addfood');
    });
});



Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function()
{
    Route::prefix('bill')->name('bill.')->group(function()
    {
        Route::get('index','BillController@index')->name('index');
        Route::post('index/{id}','BillController@show')->name('show');
        
        Route::get('create','BillController@create')->name('create');
        Route::post('store','BillController@store')->name('store');

        Route::get('edit/{id}','BillController@edit')->name('edit');
        Route::post('update/{id}','BillController@update')->name('update');

        Route::get('destroy/{id}','BillController@destroy')->name('destroy');

        Route::get('list/{table_id}','BillController@list')->name('list');
    });
});



Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function()
{
    Route::prefix('billinfo')->name('billinfo.')->group(function()
    {
        Route::get('index','BillinfoController@index')->name('index');
        
        Route::get('create','BillinfoController@create')->name('create');
        Route::post('store','BillinfoController@store')->name('store');

        Route::get('edit/{id}','BillinfoController@edit')->name('edit');
        Route::post('update/{id}','BillinfoController@update')->name('update');

        Route::get('destroy/{id}','BillinfoController@destroy')->name('destroy');


        Route::get('list/{bill_id}','BillinfoController@list')->name('list');
    });
});


Route::fallback(function () {
    echo "The website on the link you searched for does not exist";
});



