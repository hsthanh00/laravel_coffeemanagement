<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB,Datetime;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $table = DB::table('table')->get();
       return View('cpadmin.modules.table.index')->with('table',$table);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('cpadmin.modules.table.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function tableNameNotExists($name)
    {
        $record = DB::table('table')->where('name','=',$name)->get(); // get() trả ra 1 mảng object
        if(count($record) == 0)
        {
                return true;
        }
        else
        {
                return false;
        }
    }

    private function myFormatTableName($name)
    {
        $pattern = '/^[0-9]+$/';
        if(preg_match($pattern,$name))
        {
            return 1 ;
        }
        else
        {
            return 0 ;
        }
    }

    public function store(Request $request)
    {
          $name = $request->name;
            
          if($this->myFormatTableName($name) == 1)
          {
            if($this->tableNameNotExists($name) == true)
            {    
                $data = $request->except('_token');
                $data['created_at'] = new Datetime;
                $data['updated_at'] = new Datetime;
                DB::table('table')->insert($data);
                return redirect()->route('admin.table.create');
                
            }
            else
            {
                echo "Vui Lòng Nhập Lại Tên Khác";
                echo "<br>";
                echo "Tên Bàn Này $name Đã Tồn Tại";
            }
          }
          else
          {
            echo "Vui Lòng Nhập Đúng Format";
            echo "<br>";
            echo "Tên Bàn Chỉ Cho Phép Nhập Số";
          }
           
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('table')->where('id',$id)->first();// first trả ra 1 object 
        return View('cpadmin.modules.table.edit',['table'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        if($this->myFormatTableName($name) == 1)
        {
            if($this->tableNameNotExists($name) == true)
            {
                $data = $request->except('_token');
                $data['updated_at'] = new Datetime;
                DB::table('table')->where('id',$id)->update($data);
                return redirect()->route('admin.table.index');
            }
            else
            {
                echo "Vui Lòng Nhập Lại Tên Khác";
                echo "<br>";
                echo "Tên Bàn Này $name Đã Tồn Tại";
            }
        }
        else
        {
            echo "Vui Lòng Nhập Đúng Format";
            echo "<br>";
            echo "Tên Bàn Chỉ Cho Phép Nhập Số";
        }
       
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('table')->where('id',$id)->delete();
        return redirect()->route('admin.table.index');
    }


    public function pay($id)
    {
        return "Chức năng làm sau";
    }

    public function cancle($id)
    {
        return "Chức năng làm sau"; 
    }
}
