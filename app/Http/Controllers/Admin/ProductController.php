<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB , Datetime;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index($category_Id)
    {   
        // select * from product where category_id = $category_Id and where status = 0 ;
        $object_Category = DB::table('category')->where('id','=',$category_Id)->first();
        $data = DB::table('product')->where('category_id','=',$category_Id)->where('status','=',0)->get();
        return View('cpadmin.modules.product.index',['product'=>$data],['object_Category'=>$object_Category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category_Id)
    {
        // select name form category where id = $category_Id;
        $category_Name = DB::table('category')->where('id','=',$category_Id)->value('name');
        return View('cpadmin.modules.product.create',['category_Id'=>$category_Id] ,['category_Name'=> $category_Name]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function productNameNotExists($name)
    {   
       // select id form `product` where `name` = $name;
        $record = DB::table('product')->where('name','=',$name)->get('id');
        if(count($record) == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private function myFormatProductName($name)
    {
        $pattern =  '/^[aàảãáạăằẳẵắặâầẩẫấậbcdđeèẻẽéẹêềểễếệfghiìỉĩíịjklmnoòỏõóọôồổỗốộơờởỡớợpqrstuùủũúụưừửữứựvwxyỳỷỹýỵz0-9_ ]+$/';
        if(preg_match($pattern,$name))
        {
            return 1 ;
        }
        else
        {
            return 0;
        }
    }
    private function myFormatProductPrice($price)
    {
        $pattern = '/^[0-9]+$/'; 
        if(preg_match($pattern,$price))
        {
            return 1 ;
        }
        else
        {
            return 0;
        }
    }

    public function store(Request $request)
    {
         //insert into `product`(`name`,`category_id`,`price`) values()
        
        
         $name = $request->name;
         $price = $request->price;
         if($this->myFormatProductName($name) == 1)
         {
             if($this->myFormatProductPrice($price) == 1)
             {
                $Result = $this->productNameNotExists($name);
                if($Result == true)
                {
                     $data = $request->except('_token');
                     $data['created_at'] = new Datetime;
                     $data['updated_at'] = new Datetime;
                     DB::table('product')->insert($data);
                return redirect()->route('admin.product.create',['category_Id'=>$request->category_Id]);
                }
                else
                {
                    echo "Vui Lòng Nhập Lại Tên Khác";
                    echo "<br>";
                    echo "Tên Sản Phẩm $name Đã Tồn Tại";
                }
            }
            else
            {
                echo "Vui Lòng Nhập Đúng Format";
                echo "<br>";
                echo "Tên Sản Phẩm Không Được Nhập Ký Tự Đặc Biệt";
            }
         }
         else
         {
            echo "Vui Lòng Nhập Lại Trường Tên";
            echo "<Br>";
            echo " Tên Không Cho Phép Nhập Ký Tự Đặt Biệt ";
         }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            // Get Name And Id Of Category
            // truy ngược 
            // Bằng Cách join category with product
            // select A.name , A.ID from category as A join product as B on A.ID = B.category_id
            //  where B.ID = $id ; 
            $object_Category = DB::table('category')
            ->join('product','category.id','=','product.category_id')
            ->select('category.name','category.id')
            ->where('product.id','=',$id)
            ->first(); // first trả ra 1 object not 1 array object
            $data = DB::table('product')->where('id','=',$id)->where('status','=',0)->first();
            return View('cpadmin.modules.product.edit',['product'=>$data],['object_Category'=>$object_Category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $price = $request->price;
        if($this->myFormatProductName($name) == 1)
        {
            if($this->myFormatProductPrice($price) == 1)
            {
               $Result = $this->productNameNotExists($name);
               if($Result == true)
               {
                    $data = $request->except('_token');
                    $data['updated_at'] = new Datetime;
                    DB::table('product')->where('id','=',$id)->update($data);
               return redirect()->route('admin.product.create',['category_Id'=>$request->category_Id]);
               }
               else
               {
                   echo "Vui Lòng Nhập Lại Tên Khác";
                   echo "<br>";
                   echo "Tên Sản Phẩm $name Đã Tồn Tại";
               }
           }
           else
           {
               echo "Vui Lòng Nhập Đúng Format";
               echo "<br>";
               echo "Tên Sản Phẩm Không Được Nhập Ký Tự Đặc Biệt";
           }
        }
        else
        {
           echo "Vui Lòng Nhập Lại Trường Tên";
           echo "<Br>";
           echo " Tên Không Cho Phép Nhập Ký Tự Đặt Biệt ";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object_Category = DB::table('category')
        ->join('product','category.id','=','product.category_id')
        ->where('product.id','=',$id)
        ->select('category.name','category.id')
        ->first(); 
        DB::table('product')->where('id','=',$id)->delete();
        return redirect()->route('admin.product.index',['category_Id'=>$object_Category->id]);
    }

    public function addfood($table_id)
    {
        return "Chức Năng Này Làm Sau";
    }
}
