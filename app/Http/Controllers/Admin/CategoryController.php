<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB,Datetime;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($table_id)
    {
        $data = DB::table('category')->get();
        // dd($data);
        return View('cpadmin.modules.category.index' ,['category' => $data ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('cpadmin.modules.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function myFormatCategoryName($name)
    { 
        $pattern ='/^[aàảãáạăằẳẵắặâầẩẫấậbcdđeèẻẽéẹêềểễếệfghiìỉĩíịjklmnoòỏõóọôồổỗốộơờởỡớợpqrstuùủũúụưừửữứựvwxyỳỷỹýỵz0-9_ ]+$/';
        if(preg_match($pattern,$name)) 
        {
            return 1 ; 
        }
        else 
        {
            return 0;
        }
    }
    private function categoryNameNotExists($name , $parent)
    {
        $record = DB::table('category')->where('name','=',$name)->where('parent','=',$parent)->get();
        if(count($record) == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function store(Request $request)
    {
        $name = $request->name;
        $parent = $request->parent; 
        // dd($parent);
        if($this->myFormatCategoryName($name) == 1)
        {
            $result = $this->categoryNameNotExists($name,$parent);
            if($result == true)
            {
                $data = $request->except('_token');
                $data['created_at'] = new Datetime;
                $data['updated_at'] = new Datetime;
                DB::table('category')->insert($data);
                return redirect()->route('admin.category.create');
            }
            else
            {
               echo "Vui Lòng Nhập Lại Tên Khác";
               echo "<br>";
               echo "Tên Danh Mục Này $name Đã Tồn Tại";
            }
        }
        else
        {
            echo "Vui Lòng Nhập Đúng Format";
            echo "<br>";
            echo "Tên Danh Mục Không Được Có Ký Tự Đặc Biệt";
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('category')->where('id',$id)->first();
        return View('cpadmin.modules.category.edit',['category'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $parent = $request->parent;
        if($this->myFormatCategoryName($name) == 1)
        {
            if($this->categoryNameNotExists($name,$parent) == true)
            {
                $data = $request->except('_token');
                $data['updated_at'] = new Datetime;
                DB::table('category')->where('id',$id)->update($data);
                return redirect()->route('admin.category.index');
            }
            else
            {
               echo "Vui Lòng Nhập Lại Tên Khác";
               echo "<br>";
               echo "Tên Danh Mục Này $name Đã Tồn Tại";
            }
        }
        else
        {
            echo "Vui Lòng Nhập Đúng Format";
            echo "<br>";
            echo "Tên Danh Mục Không Được Có Ký Tự Đặc Biệt";
        }
       
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('category')->where('id',$id)->delete();
        return redirect()->route('admin.category.index');
    }
}
