<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB,DateTime;
class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('bill')->get();
        return view('cpadmin.modules.bill.index',['bills'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cpadmin.modules.bill.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data  = $request->except('_token');
        $data['created_at'] = new DateTime;
        $data['updated_at'] = new DateTime;
        $data['cost'] = 0;
        $data['datetimecheckout'] = new DateTime;
        $data['datetimecheckin'] = new DateTime;
        $data = DB::table('bill')->insert($data);
        return redirect()->route('admin.bill.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data =DB::table('bill')->where('id',$id)->first();
        return view('cpadmin.modules.bill.edit',['billesssssssss'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data  = $request->except('_token');
      
      
        $data['cost'] = 0;
        
        $data['updated_at'] = new DateTime;
        $data = DB::table('bill')->where('id',$id)->update($data);
        return redirect()->route('admin.bill.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = DB::table('bill')->where('id',$id)->delete();
        return redirect()->route('admin.bill.index');
    }

    public function list($table_id)
    {
        $bill = DB::table('bill')->where('table_id','=',$table_id)->where('status','=',0)->first();
        return View('cpadmin.modules.bill.list',['bill'=>$bill]);
    }
}
