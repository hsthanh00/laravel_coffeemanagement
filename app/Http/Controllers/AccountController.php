<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datetime;
use DB;

class AccountController extends Controller
{
    
    public function signin()
    {
        return View('cpadmin.modules.account.signin');
    }

    
    public function login(Request $request)
    {
        $request->except('_token');
        $email = $request['email'];
        $password = $request['password'] ;
        $record = DB::table('users')->where('email','=',$email)->where('password','=',$password)->first();
        if($record != null)
        {
            return  redirect()->route('admin.table.index');
        }
        else
        {
            return View('cpadmin.modules.account.signin',['erro' =>'Something was wrong']);
        }
    }

    public function signup()
    {
        return View('cpadmin.modules.account.signup');
    }

    public function join(Request $request)
    {
        $data = $request->except('_token','repassword');
        $data['level'] = '1' ; // default level = 1 ;
        $data['created_at'] = new datetime;
        $data['updated_at'] = new datetime;
        DB::table('users')->insert($data);
        return redirect()->route('signin');
    }

    public function forgot()
    {
        return View('cpadmin.modules.account.forgot');
    }

    public function sentpassword(Requestt $request)
    {

    }
    
}
