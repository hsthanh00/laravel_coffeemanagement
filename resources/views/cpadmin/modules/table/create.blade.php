@extends('cpadmin.master')
@section('title','Create Table')
@section('content')
<form action="{{route('admin.table.store')}}" method ="POST">
    @csrf
    <div class ="container bodyinfo">
        <div class = "row justify-content-center">
            <div class ="col-xl-9 col-lg-9 ">
                <div id="bg-purple" class ="card">
                    <div class ="card-header">
                        <div class="card-title text-center">
                            Thêm Bàn 
                        </div>
                    </div>
                    <div  class ="card-body">
                         <div class="form-row">
                            <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                <div class ="form-group">
                                <label for="name">Tên Bàn</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Example : 1 ">
                                </div> 
                             </div>   
                         </div>
                    </div>
                    <div class ="card-footer ">
                            <div class="form-row">
                                <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                                  <div class ="form-group"> 
                                    <input type="submit" value="submit"class="btn btn-plum form-control">
                                  </div>
                                </div>
                                <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                                    <div class="form-row">
                                     <a href="{{route('admin.table.index')}}"class="form-control btn btn-plum">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>   
@endsection