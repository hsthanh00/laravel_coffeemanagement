@extends('cpadmin.master')
@section('content')
<div class="container-fliud">
    <div class="row justify-content-center">
        <div class="col-9 col-sm-9 col-lg-9 col-xl-9">
            <form action="{{route('admin.table.update',['id'=>$table->id])}}" method="POST">
                @csrf
                <div id="bg-purple" class="card">
                    <div class="card-header">
                        <div class="card-title text-center">
                            Chỉnh Sửa <span class="text-primary">{{$table->name}}</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="form-row">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control" value="{{$table->name}}">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-12 col-sm-12 col-lg-6 col-xl-6 text-center">
                                    <input type="submit" value="submit" class="btn form-control btn-plum">
                                </div>
                                <div class="col-12 col-sm-12 col-lg-6 col-xl-6  text-center">
                                    <a href="{{route('admin.table.index')}}" class="btn  form-control btn-plum">Back</a>
                                </div>         
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection