@extends('cpadmin.master')
@section('title','Danh Sách Bàn')
@section('content')
    <div class ="container-fluid">
        <div class ="row">
            <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                <div id="bg-purple" class="card" >
                    <div class="card-header">
                        <div class="row">
                            <div class="col-12 col-xl-12 col-sm-12 col-lg-12">
                                <div class="title text-center">    
                                    Danh Sách Bàn    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6  col-sm-6  col-xl-auto col-lg-auto text-center">
                               <a href="{{route('admin.table.create')}}" class="btn btn-plum  w-auto">Tạo Bàn</a>
                            </div>
                        </div>
                    </div>  
                    <div class="card-body">
                        <table id="example1"  class="table table-bordered">
                            <thead>
                                <tr>
                                    <th title="Số Thứ Tự">STT</th>
                                    <th title="Số ID Bàn">ID</th>
                                    <th title="Tên Bàn ">Tên Bàn</th>
                                    <th title="Trạng Thái">Trạng Thái</th>
                                    <th title="Tổng Tiền">Tổng Tiền</th>
                                    <th title="Thêm Món">Thêm Món</th>
                                    <th title="Xem Hóa Đơn">Xem Hóa Đơn </th>
                                    <th title="Thanh Toán">Thanh Toán Hóa Đơn</th>
                                    <th title="Hủy Bàn">Hủy Hóa Đơn</th>
                                    <th title="Chỉnh Sửa Bàn">Chỉnh Sửa Bàn</th>
                                    <th title="Xóa Bàn">Xóa Bàn</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($table as $tb)
                                 @if($tb->status == 1)
                                 <tr class="bg-plum">
                                   <td>{{$loop->iteration}}</td>
                                   <td>{{$tb->id}}</td>
                                   <td>{{$tb->name}}</td>
                                   <td> Có Người</td>
                                   <td>Chức năng làm sau</td>
                                   <td><a href="{{route('admin.category.index',['table_id'=>$tb->id])}}">Thêm Món</a></td>
                                   <td><a href="{{route('admin.bill.list',['table_id'=>$tb->id])}}">Xem Hóa Đơn</a></td>
                                   <td><a href="{{route('admin.table.pay',['id'=>$tb->id])}}">Thanh Toán Hóa Đơn</a></td>
                                   <td><a href="{{route('admin.table.cancle',['id'=>$tb->id])}}">Hủy Hóa Đơn</a></td>
                                   <td><a href="{{route('admin.table.edit',['id' => $tb->id])}}">Chỉnh sửa</a></td>
                                   <td><a href="{{route('admin.table.destroy',['id' => $tb->id])}}" onclick="return acceptDelete('Bạn Có Chắc Muốn Xóa Không')">Xóa Bàn</a></td>
                                </tr>
                                @else
                                <tr >
                                   <td>{{$loop->iteration}}</td>
                                   <td>{{$tb->id}}</td>
                                   <td>{{$tb->name}}</td>
                                   <td> Trống</td>
                                   <td>Chức Năng làm Sau</td>
                                   <td><a href="{{route('admin.category.index',['table_id'=>$tb->id])}}">Thêm Món</a></td>
                                   <td><a href="{{route('admin.bill.list',['table_id'=>$tb->id])}}">Xem Hóa Đơn</a></td>
                                   <td><a href="{{route('admin.table.pay',['id'=>$tb->id])}}">Thanh Toán Hóa Đơn</a></td>
                                   <td><a href="{{route('admin.table.cancle',['id'=>$tb->id])}}">Hủy Hóa Đơn</a></td>
                                   <td><a href="{{route('admin.table.edit',['id' => $tb->id])}}">Chỉnh sửa</a></td>
                                   <td><a href="{{route('admin.table.destroy',['id' => $tb->id])}}"  onclick="return acceptDelete('Bạn Có Chắc Muốn Xóa Không')">Xóa Bàn</a></td>
                                </tr> 
                                @endif  
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>                                 
                                    <th title="Số Thứ Tự">STT</th>
                                    <th title="Số ID Bàn">ID</th>
                                    <th title="Tên Bàn ">Tên Bàn</th>
                                    <th title="Trạng Thái">Trạng Thái</th>
                                    <th title="Tổng Tiền">Tổng Tiền</th>
                                    <th title="Thêm Món">Thêm Món</th>
                                    <th title="Xem Hóa Đơn">Xem Hóa Đơn </th>
                                    <th title="Thanh Toán">Thanh Toán Hóa Đơn</th>
                                    <th title="Hủy Bàn">Hủy Hóa Đơn</th>
                                    <th title="Chỉnh Sửa Bàn">Chỉnh Sửa Bàn</th>
                                    <th title="Xóa Bàn">Xóa Bàn</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-auto">
                                <a href="{{route('admin.table.create')}}" class="btn btn-plum">Tạo Bàn</a>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
@endsection