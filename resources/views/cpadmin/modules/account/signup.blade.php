@extends('cpadmin.modules.account.zmaster')
@section('title','Signup')
@section('content')
<form action="{{route('join')}}" method="POST">
  @csrf
  <div class="container down-form-20vh">
      <div class ="form-row justify-content-center">
        <div class="col-xl-4 col-lg-4 boder-radius-5 bg-purple ">
            <div class ="form-group text-center border-bottom">
                <h1 class="text-white  ">Signup</h1>
            </div>  
            <div class ="form-group">
                <label for="phone" class="text-white font-weight-bold">Phone Number</label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter your Phone Number"> 
            </div>
            <div class="form-group">
                <label for="email" class="text-white font-weight-bold">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Enter your Email">
            </div>
            <div class="form-group">
                <label for="password" class="text-white font-weight-bold">Password</label>
                <input type="password" class="form-control" name="password" id="password" placehoder="Enter your Password">
            </div>
            <div class="form-group">
                <label for="repassword" class="text-white font-weight-bold">Enter the password again</label>
                <input type="password" class="form-control" name="repassword" id="repassword" placehoder ="Enter your Password again">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-plum my-2 form-control" value="Enter">
                <a href="{{route('signin')}}" class="btn btn-plum my-2">Back</a>
            </div>
        </div>
      </div>
  </div>
</form>
@endsection