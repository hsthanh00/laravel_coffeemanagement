<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('index/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('index/dist/index/css/singin.css')}}">

    <title>@yield('title','Erro')</title>
</head>
<body class="bg-image">
    @yield('content')

    <script style="text/javascript" src ="{{asset('index/dist/bootstrap/js/bootstrap.min.js')}}"></script>
    <script style="text/javascript" src="{{asset('index/dist/index/js/singin.js')}}"></script>
</body>
</html>