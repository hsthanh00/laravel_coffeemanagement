@extends('cpadmin.modules.account.zmaster')
@section('title','Signin')
@section('content')

    <form action="{{route('login')}} " method ="POST">
     @csrf
      <div class="container-fluid down-form-20vh">
       <div class= "form-row justify-content-center ">
         <div class ="col-xl-4 col-lg-4 boder-radius-5 bg-purple">
            <div class ="form-group  border-bottom text-align-center">
                <h1 class="text-center text-white">Signin</h1>
            </div>
            <div class = "form-group">
                    <label for="email" class="text-white font-weight-bold"> Email</label>
                    <input class="form-control" name="email" type="text" id="eamil" placeholder="Enter your Username">
            </div>
            <div class ="form-group">
                    <label for="Password" class="text-white font-weight-bold">Password</label>
                    <input class="form-control" name="password" type="password" id ="Password" placeholder="Enter your Password">
            </div>
            <div class ="form-group">
              <input type="submit" class="btn btn-plum form-control" value="Enter">
              <a href="{{route('signup')}}" class="btn btn-plum my-2"> Sign up</a>
              <a href="{{route('forgot')}}" class="btn btn-plum my-2">Forgot Password </a>
            </div>
        </div> 
      </div>
       </div>
    </form>

@endsection