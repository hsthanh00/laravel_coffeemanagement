@extends('cpadmin.master')
@section('title','Danh Sách Danh Mục')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
          <div id="bg-purple" class="card">
             <div class="card-header">
                 <div class="row">
                    <div class="col-12 col-lg-12 col-sm-12 col-xl-12">
                       <div class="card-title text-center">
                          Danh Sách Danh Mục
                       </div>
                    </div>
                 </div>
                 <div class="row">
                      <div class="col-6 col-xl-6 col-sm-auto col-lg-auto">
                           <a href="{{route('admin.category.create')}}" class="btn btn-plum">Tạo Danh Mục</a>
                     </div>
                 </div>
             </div>
             <div  class="card-body">
               <table id="example1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>ID</th>
                            <th>Tên Danh Mục</th>
                            <th>Parent</th>
                            <th>Ngày Tạo</th>
                            <th>Ngày Cập Nhật</th>
                            <th>Thêm Sản Phẩm Cho Danh Mục Này</th>
                            <th>Xem Sản Phẩm Của Danh Mục Này</th>
                            <th>Chỉnh Sửa Danh Mục</th>
                            <th>Xóa Danh Mục</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($category as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->id}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->parent}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->updated_at}}</td>
                            <td title="Thêm Sản Phẩm Cho Danh Mục {{$item->name}}"><a href="{{route('admin.product.create',['category_Id'=>$item->id])}}">Thêm</a></td>
                            <td title="Xem Sản Phẩm Cho Danh Mục {{$item->name}}"><a href="{{route('admin.product.index',['category_Id'=>$item->id])}}">Xem</a></td>
                            <th title="Chỉnh Sửa Danh Mục {{$item->name}}"><a href="{{route('admin.category.edit',['id' =>$item->id])}}" >Chỉnh Sửa</a></th>
                            <th title="Xóa Danh Mục {{$item->name}}"><a href="{{route('admin.category.destroy',['id' =>$item->id])}}" onclick="return acceptDelete('Bạn Có chắc Muốn Xóa Danh Mục Này')">Xóa</a></th>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                       <tr>
                            <th>STT</th>
                            <th>ID</th>
                            <th>Tên</th>
                            <th>Parent</th>
                            <th>Ngày Tạo</th>
                            <th>Ngày Cập Nhật</th>
                            <th>Thêm Sản Phẩm Cho Danh Mục Này</th>
                            <th>Xem Sản Phẩm Của Danh Mục Này</th>
                            <th>Chỉnh Sửa Danh Mục</th>
                            <th>Xóa Danh Mục</th>
                       </tr>
                    </tfoot>
               </table>
             </div>
             <div class="card-footer">
               <div class="row">
                  <div class="col-12 col-sm-12 col-lg-auto col-xl-auto">
                    <a href="{{route('admin.category.create')}}" class="btn btn-plum">Tạo Danh Mục</a>
                  </div>
               </div>
             </div>
          </div>
        </div>
    </div>
</div>
@endsection