@extends('cpadmin.master')
@section('title','Chỉnh Sửa Danh Mục')
@section('content')
<form action="{{route('admin.category.update',['id'=>$category->id])}}" method="POST">
@csrf
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-lg-9 col-xl-9">
          <div  id="bg-purple" class="card">
            <div class="card-header">
                <div class="card-title text-center">
                    Chỉnh Sửa Danh Mục <span class="text-primary"> {{$category->name}} </span>
                </div>
            </div>
            <div class="card-body">
                <div class="form-row">
                    <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                        <div class="form-group">
                            <label for="name">name</label>
                            <input type="text" name="name"  id="name" value="{{$category->name}}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                        <div class="form-group">
                            <label for="parent">Parent</label>
                            <select name="parent" id="parent" class="form-control">
                                <option value="1" {{($category->parent == 1)? 'selected' : ''}}>1</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-row">
                    <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                     <div class="form-group"> 
                        <input type="submit" class="btn btn-plum form-control" value="submit">
                     </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                        <div class="form-group">
                         <a href="{{route('admin.category.index')}}" class="btn btn-plum form-control">Back</a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection()