@extends('cpadmin.master')
@section('title','Tạo Danh Mục')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-lg-9 col-xl-9">
           <form action="{{route('admin.category.store')}}" method="POST">
               @csrf
              <div id="bg-purple" class="card"> 
              <div class="card-header text-center">
                  <div class="card-title">
                      Tạo Danh Mục
                  </div>
              </div>
              <div class="card-body">
                    <div class="form-group">
                       <label for="name">Tên</label>
                       <input type="text" id="name" name="name" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="parent">Parent</label>
                        <select name="parent" id="parent" class="form-control">
                            <option value="1">1</option>
                        </select>
                    </div>
               </div>
               <div class="card-footer">
                    <div class="form-row">
                        <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                            <input type="submit" value="submit" class="btn btn-plum form-control">
                        </div>
                        <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                            <a href="{{route('admin.category.index')}}" class="btn btn-plum form-control">Back</a>
                        </div>
                    </div>
               </div>
            </div>
           </form>
        </div>
    </div>
</div>
@endsection