@extends('cpadmin.master')
@section('content')
@section('title','Danh Sách Bill')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
          <div id="bg-purple" class="card">
             <div class="card-header">
                 <div class="row">
                    <div class="col-12 col-lg-12 col-sm-12 col-xl-12">
                       <div class="card-title text-center">
                          Danh Sách Bill
                       </div>
                    </div>
                 </div>
             </div>
             <div  class="card-body">
               <table id="example1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Giảm giá </th>
                            <th>Tình trạng</th>
                            <th>GHI chú</th>
                            <th>Thời gian vào</th>
                            <th>Thời gian rời khỏi</th>
                            <th>Xem Chi Tiết </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$bill->id}}</td>
                            <td>5000</td>
                            <td>
                              @if($bill->status == 1)
                                Thanh Toán
                                @else 
                                Chưa Tanh Toán
                              @endif
                            </td>
                            <td>{{$bill->note}}</td>
                            <td>{{$bill->created_at}}</td>
                            <td>{{$bill->updated_at}}</td>
                            <td><a href="{{route('admin.billinfo.list',['bill_id'=>$bill->id])}}">Xem Chi Tiết</a></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <th>ID</th>
                        <th>Giảm giá </th>
                        <th>Tình trạng</th>
                        <th>GHI chú</th>
                        <th>Thời gian Vào</th>
                        <th>Thời gian rời khỏi</th>
                        <th>Xem Chi Tiết </th>
                       </tr>
                    </tfoot>
               </table>
             </div>
             <div class="card-footer">
               <div class="row">
                  <div class="col-12 col-sm-12 col-lg-auto col-xl-auto">
                    <a href="{{route('admin.table.index')}}" class="btn btn-plum">Back</a>
                  </div>
               </div>
             </div>
          </div>
        </div>
    </div>
</div>

@endsection