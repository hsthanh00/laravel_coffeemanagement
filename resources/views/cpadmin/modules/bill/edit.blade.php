@extends('cpadmin.master')
@section('title','Edit Bill ')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-sm-12 col-lg-9 col-xl-9">
           <form action="{{route('admin.bill.update',['id'=>$billesssssssss->id])}}" method="POST">
               @csrf
              <div id="bg-purple" class="card"> 
              <div class="card-header text-center">
                  <div class="card-title">
                     Sửa Bill
                  </div>
              </div>
              <div class="card-body">
                    <div class="form-group">
                       <label for="name">Giảm giá</label>
                       <input type="text" id="name" name="discount" value="{{$billesssssssss->discount}}" class="form-control" >
                    </div>
                   
                    
                    <div class="form-group">
                        <label for="name">Ghi chú</label>
                        <input type="text" id="name" name="note"  value="{{$billesssssssss->note}}"class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="name">Table_id</label>
                        <input type="text" class="form-control" value="{{$billesssssssss->table_id}}" name="table_id" placeholder="Enter Image">
                    </div>
               </div>
               <div class="card-footer">
                    <div class="form-row">
                        <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                            <input type="submit" value="submit" class="btn btn-plum form-control">
                        </div>
                        <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                            <a href="{{route('admin.category.index')}}" class="btn btn-plum form-control">Back</a>
                        </div>
                    </div>
               </div>
            </div>
           </form>
        </div>
    </div>
</div>
@endsection