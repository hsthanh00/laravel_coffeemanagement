@extends('cpadmin.master')
@section('content')
@section('title','List Bill')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
          <div id="bg-purple" class="card">
             <div class="card-header">
                 <div class="row">
                    <div class="col-12 col-lg-12 col-sm-12 col-xl-12">
                       <div class="card-title text-center">
                          Danh Sách Bill
                       </div>
                    </div>
                 </div>
                 <div class="row">
                      <div class="col-6 col-xl-6 col-sm-auto col-lg-auto">
                           <a href="{{route('admin.bill.create')}}" class="btn btn-plum">Tạo Bill</a>
                     </div>
                 </div>
             </div>
             <div  class="card-body">
               <table id="example1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>ID</th>
                            <th>Giảm giá </th>
                        
                            <th>Tình trạng</th>
                            <th>GHI chú</th>
                           
                            <th>Thời gian tạo</th>
                            <th>Thời gian cập nhật</th>
                            <th>Chỉnh Sửa Bill</th>
                            <th>Xóa Bill</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bills as $itembills)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$itembills->id}}</td>
                            <td>5000</td>
                            <td>
                              @if($itembills->status == 1)
                                Chưa thanh toán
                                @else 
                                Thanh toán
                              @endif
                            </td>
                            <td>{{$itembills->note}}</td>
                            <td>{{$itembills->created_at}}</td>
                            <td>{{$itembills->updated_at}}</td>
                           
                            <th title="Chỉnh Sửa Bill"><a href="{{route('admin.bill.edit',['id' =>$itembills->id])}}" >Chỉnh Sửa</a></th>
                            <th title="Xóa Bill "><a href="{{route('admin.bill.destroy',['id' =>$itembills->id])}}" onclick="return acceptDelete('Bạn Có chắc Muốn Xóa Bill Này')">Xóa</a></th>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                       <tr>
                        <th>STT</th>
                        <th>ID</th>
                        <th>Giảm giá </th>
                      
                        <th>Tình trạng</th>
                        <th>GHI chú</th>
                      
                        <th>Thời gian tạo</th>
                        <th>Thời gian cập nhật</th>
                        <th>Chỉnh Sửa Bill</th>
                        <th>Xóa Bill</th>
                       </tr>
                    </tfoot>
               </table>
             </div>
             <div class="card-footer">
               <div class="row">
                  <div class="col-12 col-sm-12 col-lg-auto col-xl-auto">
                    <a href="{{route('admin.category.create')}}" class="btn btn-plum">Tạo Danh Mục</a>
                  </div>
               </div>
             </div>
          </div>
        </div>
    </div>
</div>

@endsection