@extends('cpadmin.master')
@section('title','Tạo Sản Phẩm Của Danh Mục')
@section('content')
<form action="{{route('admin.product.store')}}" method="POST">
@csrf
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-lg-9 col-xl-9">
                <div id="bg-purple" class="card">
                    <div class="card-header">
                        <div class="row ">
                           <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                <div class="card-title text-center">
                                 Tạo Sản Phẩm Của Danh Mục <span class="text-primary">{{$category_Name}} </span>
                                </div>
                           </div> 
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label for="nameOfcategory">Danh Mục</label>
                                    <select name="category_Id" id="nameOfcategory" class="form-control">
                                      <option value="{{$category_Id}}">{{$category_Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                   <label for="name">Tên</label>
                                   <input type="text" id="name" name="name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                   <label for="price">Giá</label>
                                   <input type="text" id="price" name="price" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-row">
                            <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-plum form-control" value="submit">
                                </div>
                            </div>
                            <div  class="col-12 col-sm-12 col-lg-6 col-xl-6">
                                    <div class="form-group">
                                       <a href="{{route('admin.category.index')}}" class="btn btn-plum form-control">Back</a> 
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection