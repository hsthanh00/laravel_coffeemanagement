@extends('cpadmin.master')
@section('title','Danh Sách Sản Phẩm ')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
            <div id="bg-purple" class="card">
                <div class="card-header">
                    <div class="card-title">
                        Danh Sách Sản Phẩm Của Danh Mục <span class="text-primary">{{$object_Category->name}}</span>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Gía</th>
                                <th>Ngày Tạo</th>
                                <th>Ngày Chỉnh Sửa Gần Nhất</th>
                                <th>Thêm Món</th>
                                <th>Chỉnh Sửa</th>
                                <th>Xóa</th>
                            </tr>  
                        </thead>
                        <tbody>
                            @foreach($product as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td> {{$item->price}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>{{$item->updated_at}}</td>
                                <td><a href="{{route('admin.product.addfood',['table_id'=>1])}}">Thêm Món </a></td>
                                <td><a href="{{route('admin.product.edit',['id'=>$item->id])}}">Chỉnh Sửa</a></td>
                                <td><a href="{{route('admin.product.destroy',['id'=>$item->id])}}">Xóa</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Gía</th>
                                <th>Ngày Tạo</th>
                                <th>Ngày Chỉnh Sửa Gần Nhất</th>
                                <th>Thêm Món</th>
                                <th>Chỉnh Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection