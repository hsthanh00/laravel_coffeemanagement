@extends('cpadmin.master')
@section('title','Chỉnh Sửa Sản Phẩm')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-lg-9 col-xl-9">
              <form action="{{route('admin.product.update',['id'=>$product->id])}}" method="POST">
                <div id="bg-purple" class="card">
                    <div class="card-header">
                        <div class="card-title">
                           <div class="row">
                             <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                <div class="card-title text-center">
                                    Chỉnh Sửa Sản Phẩm <span class="text-primary">{{$product->name}} </span>
                                </div>
                             </div>
                           </div>
                        </div>
                    </div>
                    <div class="card-body">
                            @csrf
                            <div class="form-row">
                                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label for="NameOfCategory">Danh Mục</label>
                                        <select name="category_Id" id="NameOfCategory" class="form-control">
                                            <option value="{{$object_Category->id}}" >{{$object_Category->name}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label for="name">Tên</label>
                                        <input type="text" name="name" id="name" class="form-control" value="{{$product->name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
                                    <div class="form-group">
                                        <label for="price">Giá</label>
                                        <input type="text" name="price" id="price" class="form-control" value={{$product->price}}>
                                    </div>
                                </div>
                            </div>
                     </div>
                    <div class="card-footer">
                        <div class="form-row">
                            <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <input type="submit" class=" form-control btn btn-plum" value="submit">
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-lg-6 col-xl-6">
                                <div class="form-group">
                                    <a href="{{route('admin.product.index',['category_Id'=>$object_Category->id])}}" class="form-control btn btn-plum">Back</a>
                                </div>               
                            </div>
                        </div>
                    </div>
                </div>
             </form>
            </div>
        </div>
    </div>
@endsection