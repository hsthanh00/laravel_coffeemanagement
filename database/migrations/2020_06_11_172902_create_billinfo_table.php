<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billinfo', function (Blueprint $table) {
            $table->id();
            $table->string('name_product');
            $table->double('price_product');
            $table->integer('count');
            $table->double('total_price');
            $table->text('note')->nullable();
            $table->boolean('status')->default(0);
            $table->unsignedBigInteger('bill_id');
            $table->foreign('bill_id')->references('id')->on('bill');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billinfo');
    }
}
