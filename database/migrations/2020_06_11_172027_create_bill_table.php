<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill', function (Blueprint $table) {
            $table->id();
            $table->integer('discount')->nullable();
            $table->double('cost'); // cost after discount 
            $table->boolean('status')->default(0);
            $table->text('note')->nullable();
            $table->datetime('datetimecheckin');
            $table->datetime('datetimecheckout');
            $table->unsignedBigInteger('table_id');
            $table->foreign('table_id')->references('id')->on('table');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill');
    }
}
